FROM node:10
ARG NODE=development
ENV NODE_ENV ${NODE}
RUN mkdir -p /usr/src/app
WORKDIR /usr/src/app
COPY . .
EXPOSE 5000
CMD [ "npx", "serve" ]
