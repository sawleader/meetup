const { openBrowser, goto, write, press, closeBrowser } = require('taiko');
(async () => {
    try {
        await openBrowser();
        await goto("127.0.0.1:5000/#/all");
        await write("taiko test automation");
        await press("Enter");
    } catch (error) {
        console.error(error);
    } finally {
        await closeBrowser();
    }
})();

const { openBrowser, goto, write, press, closeBrowser } = require('taiko');
(async () => {
    try {
        await openBrowser();
        await goto("127.0.0.1:5000/#/all");
        await write("taiko test automation2");
        await press("Enter");
    } catch (error) {
        console.error(error);
    } finally {
        await closeBrowser();
    }
})();